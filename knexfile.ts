import config from "./etc/config";

module.exports = {
  client: "pg",
  connection: config.dbConfig,
  migrations: {
    directory: "./db/migrations"
  }
};
