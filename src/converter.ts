import * as Knex from "knex";

import config from "../etc/config";
import FileServiceFactory, {
  TextFile,
  TextFileStatus
} from "./domain/textFile";
import TextFileChangeEventEmitterFactory from "./domain/notification";
import TextFileContentProvider from "./infrastructure/TextFileContentProvider";
import TextFileRepository from "./infrastructure/TextFileRepository";
import NotificationProvider from "./infrastructure/NotificationProvider";
import ConvertFileStreamWorkerPoolFactory from "./domain/transformationWorker";

const notificationProvider = new NotificationProvider(config.dbConfig);
const fileAddNotification = TextFileChangeEventEmitterFactory(
  notificationProvider
);

fileAddNotification.onTextFileStatusChange((textFile: TextFile) => {
  if (textFile.status === TextFileStatus.WAITING) {
    convertFileStreamWorkerPool.addTextFile(textFile);
  }
});

const convertFileStreamWorkerPool = ConvertFileStreamWorkerPoolFactory(config);

const knex = Knex({
  client: "pg",
  connection: config.dbConfig
});

const fileService = FileServiceFactory(
  new TextFileContentProvider(
    config.file.textFilePath,
    config.file.convertedTextFilePath
  ),
  new TextFileRepository(knex)
);

async function addFileToConvert() {
  const textFiles = await fileService.getWaitingTextFiles();

  textFiles.forEach(async textFile => {
    convertFileStreamWorkerPool.addTextFile(textFile);
  });
}

addFileToConvert();
