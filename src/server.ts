import * as express from 'express';

import * as Busboy from 'busboy';

import * as Knex from 'knex';

import config from '../etc/config';

import TextFileServiceFactory, { TextFile } from './domain/textFile';
import TransformationFactory from './domain/transformation';
import TextFileContentProvider from './infrastructure/TextFileContentProvider';
import TextFileRepository from './infrastructure/TextFileRepository';

if (typeof config.server.port !== 'number') {
    throw new Error('Port in config is required');
}

const transformStreamFactory = TransformationFactory();

var knex = Knex({
  client: 'pg',
  connection: config.dbConfig
});

const textFileService = TextFileServiceFactory(
  new TextFileContentProvider(config.file.textFilePath, config.file.convertedTextFilePath),
  new TextFileRepository(knex)
);

const app = express();

app.set('port', config.server.port);

app.get('/download', async function(req, res){
  const fileId = parseInt(req.query.id);

  if (typeof fileId !== 'number') {
    res.json({
        success: false,
        message: 'id is required param'
    });
    return;
  }

  const textFileReadStreamAfterTransformation = await textFileService.getTextFileReadStreamAfterTransformation(fileId);
  const textFile = await textFileService.getTextFileById(fileId);

  if (textFile === undefined || textFileReadStreamAfterTransformation === undefined) {
    res.json({
      success: false,
      message: 'File not exists'
    });
  } else {
    res.setHeader('Content-disposition', `attachment; filename=${textFile.name}.txt`);
    res.setHeader('Content-type', 'text/plain');

    textFileReadStreamAfterTransformation.pipe(res);
  }
});

app.get('/status', async function(req, res) {
  const fileId = parseInt(req.query.id);
  if (typeof fileId !== 'number') {
    res.json({
        success: false,
        message: 'id is required param'
    });
    return;
  }

  const textFile = await textFileService.getTextFileById(fileId);

  if (textFile === undefined) {
    res.json({
      success: false,
      message: 'File not exists'
    });
  } else {
    res.json({
      success: true,
      status: textFile.status
    });
  }
});

app.post('/upload',function(req,res){
  var transformation: string = req.query.transformation || 'default';

  if (!transformStreamFactory.isTransformationExists(transformation)) {
    res.json({
      success: false,
      message: 'transformation value is incorrect'
    });
    return;
  }

  const busboy = new Busboy({
    headers: req.headers
  });
  var promises: Array<Promise<TextFile>> = [];

  busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    if (textFileService.isMimetypePermitted(mimetype)) {
      promises.push(textFileService.addTextFile(fieldname, transformation, file));
    } else {
      file.resume();
    }
  });

  busboy.on('finish', async function() {
    const result = await Promise.all(promises);
    res.setHeader('Connection', 'close');
    res.json({
      success: true,
      fileIds: result.map(convertField => {
        return convertField.id
      })
    });
  });

  return req.pipe(busboy);
});

app.listen(app.get('port'), function () {
});
