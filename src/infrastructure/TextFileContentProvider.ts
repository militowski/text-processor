import { Readable, Writable } from "stream";

import * as fs from "fs";

import { TextFileContentProviderInterface } from "../domain/textFile";

class TextFileContentProvider implements TextFileContentProviderInterface {
  constructor(
    private textFilePath: string,
    private convertedTextFilePath: string
  ) {}
  getTextFileReadStreamAfterTransformation(uuid: string): Readable {
    return fs.createReadStream(`${this.convertedTextFilePath}/${uuid}`, {
      encoding: "utf8"
    });
  }
  getTextFileWriteStreamAfterTranformation(uuid: string): Writable {
    return fs.createWriteStream(`${this.convertedTextFilePath}/${uuid}`, {
      encoding: "utf8"
    });
  }
  getTextFileReadStreamBeforeTransformation(uuid: string): Readable {
    return fs.createReadStream(`${this.textFilePath}/${uuid}`, {
      encoding: "utf8"
    });
  }
  getTextFileWriteStreamBeforeTransformation(uuid: string): Writable {
    return fs.createWriteStream(`${this.textFilePath}/${uuid}`, {
      encoding: "utf8"
    });
  }
  async deleteTextFileBeforeTransformation(uuid: string): Promise<void> {
    return fs.unlink(`${this.textFilePath}/${uuid}`, () => {});
  }
}

export default TextFileContentProvider;
