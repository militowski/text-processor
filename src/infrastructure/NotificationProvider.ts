import * as pg from "pg";

import { NotificationProviderInterface } from "../domain/notification";

class NotificationProvider implements NotificationProviderInterface {
  private client: pg.Client;
  private listeners: Array<(notificationName: string, payload: any) => void>;
  constructor(bdConfig: Object) {
    this.listeners = [];
    this.client = new pg.Client({
      ...bdConfig,
      keepAlive: true
    });
    this.startListen();
  }
  async startListenOnEvent(eventName: string): Promise<void> {
    await this.client.query(`LISTEN ${eventName}`);
  }
  onNotification(
    listener: (notificationName: string, payload: any) => void
  ): this {
    this.listeners.push(listener);

    return this;
  }
  private async startListen(): Promise<void> {
    await this.client.connect();
    this.client.on("notification", async data => {
      this.listeners.forEach(listener => {
        if (data.payload) {
          listener(data.channel, JSON.parse(data.payload));
        }
      });
    });
  }
}

export default NotificationProvider;
