import * as Knex from "knex";

import {
  TextFile,
  TextFileStatus,
  TextFileRepositoryInterface
} from "../domain/textFile";

class TextFileRepository implements TextFileRepositoryInterface {
  private tableName = "file_transformation.text_files";
  constructor(private knex: Knex) {}
  async addTextFile(
    uuid: string,
    name: string,
    transformation: string,
    status: TextFileStatus
  ): Promise<TextFile> {
    const result = await this.knex(this.tableName)
      .insert({
        uuid: uuid,
        name: name,
        status: status,
        transformation_name: transformation
      })
      .returning("*");

    const file = result[0];

    return {
      id: file.id,
      uuid: file.uuid,
      name: file.name,
      createdAt: file.created_at,
      status: file.status,
      transformationName: file.transformation_name
    };
  }
  async getTextFileById(id: number): Promise<TextFile | undefined> {
    const result = await this.knex(this.tableName)
      .select()
      .where("id", id);

    if (!result[0]) {
      return;
    }

    const file = result[0];

    return {
      id: file.id,
      uuid: file.uuid,
      name: file.name,
      createdAt: file.created_at,
      status: file.status,
      transformationName: file.transformation_name
    };
  }
  async updateTextFile(textFile: TextFile): Promise<void> {
    const result = await this.knex(this.tableName)
      .update({
        name: textFile.name,
        status: textFile.status
      })
      .where("id", textFile.id);
  }
  async getWaitingTextFiles(): Promise<Array<TextFile>> {
    const result = await this.knex(this.tableName)
      .select()
      .where("status", "WAITING");

    return result.map(file => {
      return {
        id: file.id,
        uuid: file.uuid,
        name: file.name,
        createdAt: file.created_at,
        status: file.status,
        transformationName: file.transformation_name
      };
    });
  }
}

export default TextFileRepository;
