import TextFileChangeEventEmitter from "./eventEmitter/TextFileChangeEventEmitter";
import NotificationProviderInterface from "./interface/NotificationProviderInterface";
import TextFileChangeEventEmitterInterface from "./interface/TextFileChangeEventEmitterInterface";

export default function(
  notificationProviderInterface: NotificationProviderInterface
): TextFileChangeEventEmitterInterface {
  return new TextFileChangeEventEmitter(notificationProviderInterface);
}
export {
  NotificationProviderInterface
};
