import { TextFile } from "src/domain/textFile";

interface TextFileChangeEventEmitterInterface {
  onTextFileStatusChange(listener: (textFile: TextFile) => void): void;
}

export default TextFileChangeEventEmitterInterface;
