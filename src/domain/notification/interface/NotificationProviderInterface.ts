interface NotificationProviderInterface {
  startListenOnEvent(eventName: string): Promise<void>;
  onNotification(
    listener: (notificationName: string, payload: any) => void
  ): void;
}

export default NotificationProviderInterface;
