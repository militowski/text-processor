import TextFileChangeEventEmitterInterface from "../interface/TextFileChangeEventEmitterInterface";
import NotificationProviderInterface from "../interface/NotificationProviderInterface";
import { EventEmitter } from "events";
import { TextFile } from "src/domain/textFile";

class TextFileChangeEventEmitter extends EventEmitter
  implements TextFileChangeEventEmitterInterface {
  private textFileStatusChanged: string = "text_file_status_changed";
  constructor(
    private notificationProviderInterface: NotificationProviderInterface
  ) {
    super();

    this.notificationProviderInterface.startListenOnEvent(
      this.textFileStatusChanged
    );
    this.notificationProviderInterface.onNotification(
      async (notificationName: string, payload: any) => {
        if (notificationName === this.textFileStatusChanged) {
          const textFile: TextFile = {
            id: payload.id,
            uuid: payload.uuid,
            name: payload.name,
            createdAt: new Date(payload.created_at),
            status: payload.status,
            transformationName: payload.transformationName
          };

          this.emit(notificationName, textFile);
        }
      }
    );
  }
  onTextFileStatusChange(listener: (textFile: TextFile) => void): void {
    super.on(this.textFileStatusChanged, listener);
  }
}

export default TextFileChangeEventEmitter;
