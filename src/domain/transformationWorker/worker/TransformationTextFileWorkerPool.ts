import { TextFile } from "src/domain/textFile";

import { Worker } from "worker_threads";

import TransformationTextFileWorkerPoolInterface from "../interface/TransformationTextFileWorkerPoolInterface";

class TransformationTextFileWorkerPool
  implements TransformationTextFileWorkerPoolInterface {
  workers: Array<Worker>;
  constructor(
    private config: {
      workersCount: number;
      file: {
        textFilePath: string;
        convertedTextFilePath: string;
      }
    }
  ) {
    this.workers = new Array<Worker>();
    for (let i = 0; i < config.workersCount; i++) {
      this.workers.push(
        new Worker(__dirname + "/TransformationTextFileWorker.js", {
          workerData: this.config
        })
      );
    }
  }
  addTextFile(textFile: TextFile) {
    const worker = this.workers[textFile.id % this.config.workersCount];
    worker.postMessage(textFile);
  }
}

export default TransformationTextFileWorkerPool;
