import { TextFile } from "src/domain/textFile";
import { Writable, WritableOptions } from "stream";

import { TransformationFactoryInterface } from "../../transformation";
import { TextFileServiceInterface } from "../../textFile";

class TransformTextFileQueue extends Writable {
  private textFileService: TextFileServiceInterface;
  private transformationFactory: TransformationFactoryInterface;
  constructor(
    textFileService: TextFileServiceInterface,
    transformationFactory: TransformationFactoryInterface,
    opts?: WritableOptions
  ) {
    if (!opts) {
      opts = {};
    }
    opts.objectMode = true;
    super(opts);

    this.textFileService = textFileService;
    this.transformationFactory = transformationFactory;
  }
  _write(
    textFile: TextFile,
    encoding: string,
    callback: (error?: Error | null) => void
  ) {
    const transformStream = this.transformationFactory.getTransformation(
      textFile.transformationName
    );

    if (transformStream === undefined) {
      callback(
        new Error(`Transformation ${textFile.transformationName} not exists`)
      );
      return;
    }

    this.textFileService
      .transformTextFile(textFile.id, transformStream)
      .then(() => {
        callback(null);
      });
  }
}

export default TransformTextFileQueue;
