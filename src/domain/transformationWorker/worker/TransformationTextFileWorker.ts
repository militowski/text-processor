import * as Knex from "knex";

import { parentPort, workerData } from "worker_threads";

import TextFileServiceFactory, { TextFile } from "../../textFile";
import TransformStreamFactory from "../../transformation";
import TextFileContentProvider from "../../../infrastructure/TextFileContentProvider";
import TextFileRepository from "../../../infrastructure/TextFileRepository";

import transformTextFileQueue from "./TransformTextFileQueue";

const knex = Knex({
  client: "pg",
  connection: workerData.dbConfig
});

const textFileRepository = new TextFileRepository(knex);
const textFileContentProvider = new TextFileContentProvider(
  workerData.file.textFilePath,
  workerData.file.convertedTextFilePath
);
const textFileService = TextFileServiceFactory(
  textFileContentProvider,
  textFileRepository
);

const transformStreamFactory = TransformStreamFactory();
const convertFileStream = new transformTextFileQueue(
  textFileService,
  transformStreamFactory
);

if (parentPort) {
  parentPort.on("message", (textFile: TextFile) => {
    convertFileStream.write(textFile);
  });
} else {
  throw new Error(
    "TransformationTextFileWorker can be use only in worker thread"
  );
}
