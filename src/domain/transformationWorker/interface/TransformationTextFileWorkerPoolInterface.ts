import { TextFile } from "src/domain/textFile";

interface TransformationTextFileWorkerPoolInterface {
  addTextFile(textFile: TextFile): void;
}

export default TransformationTextFileWorkerPoolInterface;
