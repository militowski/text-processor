import ConvertFileStreamWorkerPool from './worker/TransformationTextFileWorkerPool';
import ConvertFileStreamWorkerPoolInterface from './interface/TransformationTextFileWorkerPoolInterface';

export default function (
  config: {
    workersCount: number;
    file: {
      textFilePath: string;
      convertedTextFilePath: string;
    }
  }
): ConvertFileStreamWorkerPoolInterface {
  return new ConvertFileStreamWorkerPool(config);
}
