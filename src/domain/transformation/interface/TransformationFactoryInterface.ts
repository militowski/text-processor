import { Transform } from "stream";

interface TransformationFactoryInterface {
  isTransformationExists(transformationName: string): boolean;
  getTransformation(transformationName: string): Transform | undefined;
}

export default TransformationFactoryInterface;
