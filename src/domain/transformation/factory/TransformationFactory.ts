import { Transform } from "stream";

import TransformationFactoryInterface from "../interface/TransformationFactoryInterface";

import DefaultTransformation from "./transformation/DefaultTransformation";
import OtherTransformation from "./transformation/OtherTransformation";

class TransformationFactory implements TransformationFactoryInterface {
  isTransformationExists(transformationName: string): boolean {
    return ["default", "other"].includes(transformationName);
  }
  getTransformation(transformationName: string): Transform | undefined {
    switch (transformationName) {
      case "default":
        return new DefaultTransformation();
      case "other":
        return new OtherTransformation();
    }
  }
}

export default TransformationFactory;
