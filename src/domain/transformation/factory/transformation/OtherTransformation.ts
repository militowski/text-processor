import { Transform, TransformCallback, TransformOptions } from "stream";

class OtherTransformation extends Transform {
  private regexp: RegExp;
  constructor(opts?: TransformOptions) {
    if (!opts) {
      opts = {};
    }
    (opts.decodeStrings = false), (opts.encoding = "utf8");
    super(opts);

    this.regexp = /[abc]/g;
  }
  _transform(chunk: any, encoding: string, callback: TransformCallback) {
    callback(null, chunk.replace(this.regexp, ""));
  }
}

export default OtherTransformation;
