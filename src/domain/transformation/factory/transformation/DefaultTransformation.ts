import { Transform, TransformCallback, TransformOptions } from "stream";

class DefaultTransformation extends Transform {
  private regexp: RegExp;
  constructor(opts?: TransformOptions) {
    if (!opts) {
      opts = {};
    }
    (opts.decodeStrings = false), (opts.encoding = "utf8");
    super(opts);

    this.regexp = /[$%.,]/g;
  }
  _transform(chunk: any, encoding: string, callback: TransformCallback) {
    callback(null, chunk.replace(this.regexp, ""));
  }
}

export default DefaultTransformation;
