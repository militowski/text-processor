import TransformationFactory from "./factory/TransformationFactory";
import TransformationFactoryInterface from "./interface/TransformationFactoryInterface";

export default function(): TransformationFactoryInterface {
  return new TransformationFactory();
}
export { TransformationFactoryInterface };
