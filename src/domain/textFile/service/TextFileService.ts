import { Stream, Readable, Duplex } from "stream";
import { promisify } from "util";
import * as uuid from "uuid-random";
import TextFileContentProvider from "../interface/TextFileContentProviderInterface";
import TextFileRepositotyInterface from "../interface/TextFileRepositoryInterface";
import TextFileServiceInterface from "../interface/TextFileServiceInterface";
import { TextFile, TextFileStatus } from "../type/TextFile";

class TextFileService implements TextFileServiceInterface {
  constructor(
    private textFileContentProvider: TextFileContentProvider,
    private textFileRepository: TextFileRepositotyInterface
  ) {}
  isMimetypePermitted(mimetype: string): boolean {
    return ["text/csv", "text/plain"].includes(mimetype);
  }
  async addTextFile(
    filename: string,
    transformationName: string,
    fileStream: NodeJS.ReadableStream
  ): Promise<TextFile> {
    const textFileUuid = uuid();
    const textFileWriteStreamBeforeTransformation = this.textFileContentProvider.getTextFileWriteStreamBeforeTransformation(
      textFileUuid
    );

    const finished = promisify(Stream.finished);
    await finished(fileStream.pipe(textFileWriteStreamBeforeTransformation));

    const textFile: TextFile = await this.textFileRepository.addTextFile(
      textFileUuid,
      filename,
      transformationName,
      TextFileStatus.WAITING
    );

    return textFile;
  }
  async getTextFileReadStreamAfterTransformation(
    id: number
  ): Promise<Readable | undefined> {
    const textFile = await this.textFileRepository.getTextFileById(id);

    if (!textFile) {
      return;
    }

    return this.textFileContentProvider.getTextFileReadStreamAfterTransformation(
      textFile.uuid
    );
  }
  getTextFileById(id: number): Promise<TextFile | undefined> {
    return this.textFileRepository.getTextFileById(id);
  }
  async transformTextFile(
    id: number,
    transformation: Duplex
  ): Promise<boolean> {
    const textFile:
      | TextFile
      | undefined = await this.textFileRepository.getTextFileById(id);

    if (!textFile) {
      return false;
    }

    const textFileReadStream = this.textFileContentProvider.getTextFileReadStreamBeforeTransformation(
      textFile.uuid
    );
    const textFileWriteStream = this.textFileContentProvider.getTextFileWriteStreamAfterTranformation(
      textFile.uuid
    );

    const finished = promisify(Stream.finished);
    await finished(
      textFileReadStream.pipe(transformation).pipe(textFileWriteStream)
    );

    this.textFileContentProvider.deleteTextFileBeforeTransformation(
      textFile.uuid
    );

    await this.textFileRepository.updateTextFile({
      ...textFile,
      status: TextFileStatus.READY
    });

    return true;
  }
  getWaitingTextFiles(): Promise<Array<TextFile>> {
    return this.textFileRepository.getWaitingTextFiles();
  }
}

export default TextFileService;
