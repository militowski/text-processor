enum TextFileStatus {
  WAITING = "WAITING",
  READY = "READY"
}

type TextFile = {
  id: number;
  uuid: string;
  name: string;
  createdAt: Date;
  status: TextFileStatus;
  transformationName: string;
};

export { TextFileStatus, TextFile };
