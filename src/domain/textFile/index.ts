import TextFileContentProviderInterface from "./interface/TextFileContentProviderInterface";
import TextFileRepositoryInterface from "./interface/TextFileRepositoryInterface";
import TextFileServiceInterface from "./interface/TextFileServiceInterface";
import TextFileService from "./service/TextFileService";
import { TextFile, TextFileStatus } from "./type/TextFile";

export default function(
  TextFileContentProvider: TextFileContentProviderInterface,
  TextFileRepository: TextFileRepositoryInterface
): TextFileServiceInterface {
  return new TextFileService(TextFileContentProvider, TextFileRepository);
}
export {
  TextFileContentProviderInterface,
  TextFileRepositoryInterface,
  TextFileServiceInterface,
  TextFile,
  TextFileStatus
};
