import { TextFile } from "../type/TextFile";
import { Readable, Duplex } from "stream";

interface TextFileServiceInterface {
  isMimetypePermitted(mimetype: string): boolean;
  addTextFile(
    filename: string,
    transformationName: string,
    fileStream: NodeJS.ReadableStream
  ): Promise<TextFile>;
  getTextFileReadStreamAfterTransformation(id: number): Promise<Readable | undefined>;
  getTextFileById(id: number): Promise<TextFile | undefined>;
  transformTextFile(id: number, transformation: Duplex): Promise<boolean>;
  getWaitingTextFiles(): Promise<Array<TextFile>>;
}

export default TextFileServiceInterface;
