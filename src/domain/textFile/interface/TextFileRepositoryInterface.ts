import { TextFile, TextFileStatus } from "../type/TextFile";

interface TextFileRepositoryInterface {
  addTextFile(
    uuid: string,
    name: string,
    transformation: string,
    status: TextFileStatus
  ): Promise<TextFile>;
  updateTextFile(textFile: TextFile): Promise<void>;
  getTextFileById(id: number): Promise<TextFile | undefined>;
  getWaitingTextFiles(): Promise<Array<TextFile>>;
}

export default TextFileRepositoryInterface;
