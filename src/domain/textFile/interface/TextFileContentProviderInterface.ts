import { Readable, Writable } from 'stream';

interface TextFileContentProviderInterface {
  getTextFileReadStreamBeforeTransformation(uuid: string): Readable;
  getTextFileWriteStreamBeforeTransformation(uuid: string): Writable;
  getTextFileReadStreamAfterTransformation(uuid: string): Readable;
  getTextFileWriteStreamAfterTranformation(uuid: string): Writable;
  deleteTextFileBeforeTransformation(uuid: string): Promise<void>;
}

export default TextFileContentProviderInterface;
