import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.raw(`
  CREATE OR REPLACE FUNCTION file_transformation.send_update_text_file_status()
  RETURNS trigger AS
  $BODY$
      BEGIN
          IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND OLD.status != NEW.status) THEN
             PERFORM pg_notify('text_file_status_changed', json_build_object('id',NEW.id,'name',NEW.name,'uuid',NEW.uuid,'createdAd',NEW.created_at,'status',NEW.status,'transformationName',NEW.transformation_name)::text);
          END IF;
      RETURN NEW;
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE;
    
    CREATE TRIGGER file_change
        AFTER INSERT ON file_transformation.text_files
        FOR EACH ROW
        EXECUTE PROCEDURE file_transformation.send_update_text_file_status();`);
}

export async function down(knex: Knex): Promise<any> {
  await knex.raw(`
    DROP TRIGGER file_change ON file_transformation.text_files;
    DROP FUNCTION file_transformation.send_update_text_file_status;
  `);
}
