import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable(
    "file_transformation.text_files",
    tableBuilder => {
      tableBuilder.increments("id").primary();
      tableBuilder.uuid("uuid").notNullable();
      tableBuilder.string("name", 100).notNullable();
      tableBuilder
        .timestamp("created_at")
        .notNullable()
        .defaultTo(knex.fn.now());
      tableBuilder.string("status", 100).notNullable();
      tableBuilder.string("transformation_name", 100).notNullable();
    }
  );
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable("file_transformation.text_files");
}
