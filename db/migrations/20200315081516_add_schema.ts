import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createSchema("file_transformation");
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropSchema("file_transformation");
}
