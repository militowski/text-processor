# Uruchomienie

## Config
W filderze etc należy skopiować plik **config.ts.dist** i nazwać go **config.ts**. W pliku trzeba uzupełnić wartości:

- **dbConfig** - konfiguracja połączenia dla postresql
- **file** - konfiguracja dla folderów w których są zapisywane pliki. **textFilePath** to folder w którym będą trzymane pliki przed przetworzeniem. **convertedTextFilePath** to folder w którym będą trzymane pliki po przetworzeniu
- **server** - konfiguracja dla serwera. **port** określa port na którym będzie działał serwer
- **workersCount** - określa liczbę ***worker_threads** przy przetwarzaniu plików

## Instalacja modułów

W katalogu głównym wykonujemy:

```bash
npm ci
```

## Migracje

Żeby wykonać migracje w katalogu głównym wykonujemy:

```bash
npm run migration:migrate
```
## Kompilowanie 

Żeby skompilować aplikację wykonujemy:

```bash
sudo npm install -g typescript

tsc
```
Pliki wynikowe znajdują się w katalogu **build**

## Aplikacje

- **src/server.ts** - aplikacja odpowiedzialna za uploadowanie plików, sprawdzanie statusu i pobieranie plików.
-- **src/converter.ts** - aplikacja odpowiedzialna za przetwarzanie plików


# Api

- POST /upload?transformation=**transformation**. Parametr **transformation** nie jest obowiązkowy. Określa on w jaki sposób przesłane pliki będą przetwarzane. Domyślną wartością jest **default**. Dopuszczalne wartości to **default** i **other**. Dla **default** usuwane są znaki **$%.,** ,a dla **other** znaki **abc**. Odpowiedź zawiera idki uploadowanych plików.

- GET /status?id=**id**. **id** to id pliku dla którego chcemy pobrać status

- GET /download?id=**id**. **id** pliku ,który chcemy pobrać.

# Powiadomienia

Powiadomienia są wysyłane z poziomu postgresql